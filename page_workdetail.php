<?php
session_start();
ob_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Work Detail</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link type="text/css" href="css/bootstrap.min.css" />
    <link type="text/css" href="css/bootstrap-timepicker.min.css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-timepicker.min.js"></script>
  </head>
  <body class="bg-background">
    <div class = "bg-background">
      <?php
      include "navbar.php";
      ?>
    </div>
    <div class="container p-3">
          <div class="py-5 bg-container my-5">
            <h1 class="text-center pb-3">
                Work Detail
            </h1>
            <div class="d-flex justify-content-center">
                <form class="form-horizontal" action="workdetail.php" method="post">
                <!-- Date -->
                <div class="control-group row pt-3">
                    <label class="control-label col-4" for="date">Date :</label>
                    <input id="date" width="276" name="date"/>
                </div>

                <!-- ArriveTime -->
                <div class="ccontrol-group row pt-3">
                <label class="control-label col-4" for="timepickerArrive">Arrive Time:</label>
                    <input id="timepickerArrive" width="276" type="text" name="timepickerArrive"/>
                </div>
                <!-- LeaveTime -->
                <div class="ccontrol-group row pt-3">
                <label class="control-label col-4" for="timepickerLeave">Leave Time:</label>
                    <input id="timepickerLeave" width="276" type="text" name="timepickerLeave"/>
                </div>

                <div class="control-group row pt-5 d-flex justify-content-center">
                        <button type="submit" class="btn btn-navy">Submit</button>
                    </div>
                </form>
            </div>
          </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#date').datepicker();
    </script>
    <script>
        $('#timepickerArrive').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    <script>
        $('#timepickerLeave').timepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
    </body>
</html>