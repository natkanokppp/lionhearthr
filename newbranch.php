<?php
session_start();
ob_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <title>newBranch</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link href='assets/css/select2.min.css' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>
    <script src='assets/js/select2.min.js' type='text/javascript'></script>
  </head>
  <body class="bg-background">
  <div class = "bg-background">
      <?php
      include "navbar.php";
      ?>
  </div>

      <div class="container p-3">
          <div class="py-5 bg-container my-5">
            <h1 class="text-center pb-3">
                New Branch
            </h1>
            <div class="d-flex justify-content-center">
                <form class="form-horizontal" action="addBranch.php" method="post">
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputBranchName">Branch name : </label>
                        <input class="form-control col-7" type="textarea" id="inputBranchName" name="inputBranchName" placeholder="Enter name">
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputManager">Manager's ID : </label>
                        <select class="form-control col-7" id="inputManager" style='width: 230px; padding-left: 100px;' name = "inputManager" placeholder="Select Manager's ID">
                          <option value="" disabled selected>Select Manager's ID</option>
                             <?php
                             include"connect.php";
                             $sq222 = "select EmployeeID from `employee_information` where PositionID = '2'";
                             $result = mysqli_query($con,$sq222);
                             while($row = mysqli_fetch_array($result))
                             {   
                               $id = $row['EmployeeID'];
                              echo "<option value=$id>$id</option>";
                             }
                            ?>
                        </select>
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputState">State : </label>
                        <input class="form-control col-7" type="textarea" id="inputState" name="inputState" placeholder="Enter State">
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputAddress">Address : </label>
                        <textarea class="form-control col-7" id="inputAddress" name="inputAddress" cols="30" rows="5" placeholder="Detail"></textarea>
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputPhone">Phone Number : </label>
                        <input class="form-control col-7" type="textarea" id="inputPhone" name ="inputPhone" placeholder="Enter Phone Number">
                    </div>
                    <div class="control-group row pt-5 d-flex justify-content-center">
                        <button type="submit" class="btn btn-navy">Submit</button>
                    </div>
                </form>
            </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            // Initialize select2
            $("#inputManager").select2();
    } );
    </script>
  </body>
</html>