<?php
session_start();
ob_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
	<link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="css/hover.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>
    <script>
  function Validation()
  {
    var From = document.forms["RegForm"]["datepickerStart"];
    var To = document.forms["RegForm"]["datepickerEnd"]
    var Type = document.forms["RegForm"]["inputType"];
    var Details = document.forms["RegForm3"]["inputDetail"];


    if (From.value == "") 
      {
        window.alert("Please enter start date.");
        From.focus();
        return false;
        
      }
    if (To.value == "") 
      {
        window.alert("Please enter End date");
        To.focus();
        return false;
      }
    if (Type.selectedIndex < 1) 
      {
        alert("Please select type of leave f");
        Type.focus();
        return false;
      }
    if (Details.value == "") 
      {
        window.alert("Please enter Details");
        Details.focus();
        return false;
      }
    
  }
</script>

<title>Leave Application</title>
</head>
<body class="bg-background">
  <div>
      <?php
      include "navbarEmployee.php";
      ?>
  </div>

	<div class="container bg-container">
  <div class="py-5 bg-container my-5">
  <h1 class="text-center pb-3">
			Leave Application Form
		</h1>
    <div class="d-flex justify-content-center">
   <form class="form-horizontal" action="saveLeave.php" method="post">
  <!-- Start Date -->
  <div class="ccontrol-group row pt-3">
    <label class="control-label col-4" for="datepickerStart">From:</label>
      <div class="col-8" style="padding-left: 35px;">
      <input id="datepickerStart" width="276" name="datepickerStart"/>
  </div>
</div>
  <!-- End Date -->
  <div class="control-group row pt-3">
    <label class="control-label col-4" for="datepickerEnd">To:</label>
      <div class="col-8" style="padding-left: 35px;">
      <input id="datepickerEnd" width="276" name="datepickerEnd"/>
  </div>
  </div>
   <!-- Type -->
  <div class="control-group row pt-3">
    <label class="control-label col-5" for="inputType">Type:</label>
     <select id="inputType" class="form-control col-7" name="inputType">
  <option value="business">Business</option>
  <option value="sick">Sick</option>
  <option value="vacation">Vacation</option>
</select>
  </div>
  <!-- Detail -->
  <div class="control-group row pt-3">
    <label class="control-label col-5" for="inputDetail">Details:</label>
     <textarea id="inputDetail" class="form-control col-7" rows="3" placeholder="Details" name="inputDetail"></textarea>
  </div>
  <!-- Submit -->
  <div class="control-group row pt-5 d-flex justify-content-center">
    <button type="submit" class="btn btn-navy">Submit</button>
  </div>
</form>
</div>
    </form>
    </div>
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $('#datepickerStart').datepicker();
        $('#datepickerEnd').datepicker();
    </script>
</body>
</html>