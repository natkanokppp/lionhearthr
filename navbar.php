<?php
echo '
<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="afterlogin.php">
          <img src="assets/img/building-solid.svg" width="30" height="30" class="d-inline-block align-top px-1" alt="">
          Lion Heart
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Drop down menu for HR -->
          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Employee Information
            </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="ShowProfile.php">Personal Information</a>
              <a class="dropdown-item" href="ShowActivity.php">Training</a>
              <a class="dropdown-item" href="ShowSalary.php">Salary</a>
              </div>
            </li>
          </ul>
          </nav>

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Branch
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="allbranch.php">All branch</a>
              <a class="dropdown-item" href="employeeList.php">Employee list</a>
              </div>
            </li>
          </ul>
          </nav>

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Attendance
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="page_workdetail.php">Work detail</a>
              </div>
            </li>
          </ul>
          </nav>

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Request
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="LeaveForm.php">Leave Application Form</a>
              <a class="dropdown-item" href="activity.php">Activity</a>
              <a class="dropdown-item" href="promotionchange.php">Promotion</a>
              <a class="dropdown-item" href="AddProfile1.php">Register Staff</a>
              <a class="dropdown-item" href="Payment.php">Payment</a>
              </div>
            </li>
          </ul>
          </nav>

          <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          </button>
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Analysis Report
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="mostSalary.php">Top 5 Most Salary of Department</a>
              <a class="dropdown-item" href="noStaffBranch.php">Number Staff in Each Branch</a>
              <a class="dropdown-item" href="5BestEmployee.php">Least Attendance Employee</a>
              <a class="dropdown-item" href="Worklog.php">Worklog of Employee</a>
              <a class="dropdown-item" href="Gender.php">Gender</a>
              <a class="dropdown-item" href="AverageGrade.php">Average Grade</a>
              <a class="dropdown-item" href="Age.php">Employee Age</a>
              <a class="dropdown-item" href="analyzeBonus.php">Bonus</a>
              <a class="dropdown-item" href="analyzeTrain.php">Employee Attending Course</a>
              <a class="dropdown-item" href="ShowExperiance.php">Work Experince</a>
              <a class="dropdown-item" href="CountDepartmentEachPosition.php">Count Department each Position</a>
              <a class="dropdown-item" href="Religion.php">Religion of Employee</a>
              <a class="dropdown-item" href="Nation.php">Nation of Employee</a>
              <a class="dropdown-item" href="Marital.php">Marital of Employee</a>
              </div>
            </li>
          </ul>
          </nav>
          
          <ul class="navbar-nav ml-auto">
              <li class="nav-item">
              <a class="nav-link mx-2" href="ShowProfile.php" >EmployeeID '.$_SESSION["Username"].' </a> 
              </li>
          </ul>
          <form class="form-inline my-2 my-lg-0">
            <a href="logout.php" class="btn btn-outline-dark btn-sm my-2 my-sm-0" role="button">Log out</a>
          </form>
        </div>
      </nav>
'
?>