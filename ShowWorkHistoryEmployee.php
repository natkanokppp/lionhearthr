<?php 
session_start();
ob_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Work History</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="css/hover.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  </head>
  <body class="bg-background">
    <div>
        <?php
        include "navbarEmployee.php";
        ?>
    </div>
    <div class="container p-3">
      <div class="p-5 bg-container my-5 ">
        <h1 class="text-center pb-3">
            Your Profile
        </h1>
        
          <div> 
            <?php
                $host="localhost:3306";
                $user="natkanok_hr";
                $pwd="14561";
                $dbname = "natkanok_lionhearthr";
                $con = mysqli_connect($host,$user,$pwd,$dbname);
                if(mysqli_connect_errno())
                {
                  echo "error";
                  exit();
                }
                $user = $_SESSION["Username"];
                $result1 = mysqli_query($con,"SELECT Academy,Faculty,Department,GPAX,EducationLevel FROM education_history WHERE EmployeeID = '$user'"); 
                $result2 = mysqli_query($con,"SELECT Company,Position,Reason,Duration FROM work_history WHERE EmployeeID = '$user' ");
              
            
               while($row = mysqli_fetch_array($result1))
               {
                 echo "<h1>" ."Education" ."<br>" ."</h1>";
                 echo "Academy: " .$row["Academy"]  ."<br>";
                 echo "Faculty: " .$row["Faculty"]  ."<br>";
                 echo "Department of old school: " .$row["Department"]  ."<br>";
                 echo "GPAX: " .$row["GPAX"]  ."<br>";
                 echo "Education Level: " .$row["EducationLevel"]  ."<br>";
               }
              while($row = mysqli_fetch_array($result2))
                {   
                echo "<h1>" ."Work History" ."<br>" ."</h1>";    
          
                echo "Company: " .$row["Company"]  ."<br>";
                echo "Last Position: " .$row["Position"]  ."<br>";
                echo "Reason that you resign: " .$row["Reason"]  ."<br>";
                echo "Duration: " .$row["Duration"]  ."<br>";
               }
          

                mysqli_close($con);
              ?>
              </div>
              <div class="d-flex justify-content-end">
                <a href="EditProfileEmployee.php" class="btn btn-navy" role="button">Edit</a>
              </div>
        </div>
    </div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>