<?php
session_start();
ob_start();
?>

<?php
  $_SESSION["Username"];
  $_SESSION["BanchID"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>

<script>
  function Validation()
  {
    var Firstname = document.forms["RegForm"]["FirstnameInput1"];
    var Lastname = document.forms["RegForm"]["LastnameInput2"]
    var IDCard = document.forms["RegForm"]["IDCardInput3"];
    var Phone = document.forms["RegForm3"]["InputPhoneNumber"];
    var Nationality = document.forms["RegForm"]["Nationality"];
    var Religion = document.forms["RegFrom"]["Religion"];
    var Bankaccount = document.forms["RegForm3"]["InputBankAccount"];
    var House = document.forms["RegForm3"]["HouseInput1"];
    var Road = document.forms["RegForm3"]["RoadInput2"];
    var City = document.forms["RegForm3"]["CityInput3"];
    var Country = document.forms["RegForm3"]["CountryInput4"];
    var Postal = document.forms["RegForm3"]["PostInput5"];

    if (Firstname.value == "") 
      {
        window.alert("Please enter your Firstname.");
        Firstname.focus();
        return false;
      }
    if (Lastname.value == "") 
      {
        window.alert("Please enter your Lastname");
        Lastname.focus();
        return false;
      }
    if (IDCard.value == "") 
      {
        window.alert("Please enter your ID Card number");
        IDCard.focus();
        return false;
      }
    if (Phone.value == "") 
      {
        window.alert("Please enter your Phone number")
        Phone.focus();
        return false;
      }
    if (Phone.value.indexOf("0",0) < 0) 
      {
        window.alert("Plese enter a valid Phone number");
        Phone.focus();
        return false;
      }
    if (Nationality.value == "") 
      {
        window.alert("Please enter your Nationality");
        Nationality.focus();
        return false;
      }
    if (Bankaccount.value ==  "") 
      {
        window.alert("Please enter your Bank account");
        Bankaccount.focus();
        return false;
      }
    if (House.value == "") 
      {
        window.alert("Please enter your house number");
        House.focus();
        return false;
      }
    if (Road.value == "") 
      {
        window.alert("Please enter your name or number of your road");
        Road.focus();
        return false;
      }
    if (City.value == "") 
      {
        window.alert("Please enter your City");
        City.focus();
        return false;
      }
    if (Postal.value == "") 
      {
        window.alert("Please enter your Postal in your city");
        Postal.focus();
        return false;
      }
  }
</script>

</head>
<body class="bg-background">
<div class = "bg-background">
      <?php
      include "navbar.php";
      ?>
  </div>
<!--Head web-->
<div class="container bg-background">
  <div class="py-5 px-4 bg-container my-5">
    <h1 class="text-center" id="head">
    Register
    </h1>

<!--col1 information--> 
  <div class="row">
    <div class="col-6 p-2">
      <h1>
        <img src="assets/img/user-solid.svg" width="30" height="30" alt="">
        Information
      </h1>
      <!--form link to php-->
      <form name="RegFrom" action="saveEmployeeinfo.php" onsubmit="return Validation()" method="POST">
         <div class="form-group">
            <label for="FirstnameInput1">Firstname</label>
            <input type="text" class="form-control" name = "FirstnameInput1" id="FirstnameInput1">
            <label for="LastnameInput2">Lastname</label>
            <input type="text" class="form-control" name = "LastnameInput2" id="LastnameInput2">
            <label for="IDCardInput3">ID Card</label>
            <input type="text" class="form-control" name = "IDCardInput3" id="IDCardInput3">
          </div>
        <div class="form-group">
            <label for="GenderlSelect1">Gender</label>
            <select class="form-control" name = "GenderlSelect1" id="GenderlSelect1">
             <option name = "GenderlSelect1" value="F">Female</option>
             <option name = "GenderlSelect1" value="M">Male</option>
             </select>
        </div>
        <div class="form-group">
          <label for="DateOfBirth">Date of Birth</label>
          <input name = "datepicker" id="datepicker" width="276"/>
          <label for="Nationality">Nationality</label>
          <input type="text" class="form-control" name = "Nationality" id="Nationality">
          <label for="Religion">Religion</label>
          <input type="text" class="form-control" name = "Religion" id="Religion">
        </div>
        <div class="form-group">
            <label for="MeritalSelect2">Merital</label>
            <select class="form-control" name = "MeritalSelect2" id="MeritalSelect2">
             <option id="MeritalSelect2" value = "Single" name = "MeritalSelect2">Single</option>
             <option id="MeritalSelect2" value = "Married" name = "MeritalSelect2">Married</option>
             <option id="MeritalSelect2" value = "Divorce" name = "MeritalSelect2">Divorce</option>
             </select>
        </div> 
        <div class="form-group">
        <label for="datehire">Date of Hire</label>
        <input name = "datehire" id="datehire" width="276"/>
        </div>
    </div>
    <div class="col-6 p-2">
<!--col2 picture and address-->
      <br>
      <div class="form-group">
        <label for="InputEmail1">Email address</label>
        <input type="email" class="form-control" name = "InputEmail1" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        <label for="InputPhoneNumber">Phone Number</label>
        <input type="text" class="form-control" name = "InputPhoneNumber" id="InputPhoneNumber">
        <label for="InputBankAccount">Bank Account</label>
        <input type="text" class="form-control" name = "InputBankAccount" id="InputBankAccount">
        </div>

         <h1 class="text-center">
          <img src="assets/img/home-solid.svg" width="30" height="30" alt="">
         Address</h1>
      <div class="form-group">
      <label for="HouseInput1">House No.</label>
      <input type="text" class="form-control" name = "HouseInput1" id="HouseInput1">
      <label for="RoadInput2">Road</label>
      <input type="text" class="form-control" name = "RoadInput2" id="RoadInput2">
      <label for="CityInput33">City</label>
      <input type="text" class="form-control" name = "CityInput3" id="CityInput3">
      <label for="CountryInput4">Country</label>
      <input type="text" class="form-control" name = "CountryInput4" id="CountryInput4">
      <label for="PostInput5">Postal Code</label>
      <input type="text" class="form-control" name = "PostInput5" id="PostInput5">
    </div>
    </div>
  </div>
   <h1><img src="assets/img/store-alt-solid.svg" width="30" height="30" alt="">
    Branch
      </h1>
<!--form to link php with post-->

         <div class="form-group">
          <label for="PositionSelect3">Position</label>
          <select class="form-control" name = "PositionSelect3"id="PositionSelect3">
            <option name = "PositionSelect3" id ="PositionSelect3" value = "1">Admin</option>
            <option name = "PositionSelect3" id ="PositionSelect3" value = "2">General Manager</option>
            <option name = "PositionSelect3" id ="PositionSelect3" value = "3">Human Resources</option>
            <option name = "PositionSelect3" id ="PositionSelect3" value = "4">Staff</option>
          </select>
        </div>
         <div class="form-group">
            <label for="DepartmentSelect4">Department</label>
             <select class="form-control" name = "DepartmentSelect4" id="DepartmentSelect4">
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Accounting">Accounting</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Engineering" >Engineering</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="General Manager">General Manager</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Human Resources">Human Resources</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Marketing">Marketing</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Operation">Operation</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Sales">Sale</option>
             <option name = "DepartmentSelect4" id="DepartmentSelect4" value ="Support">Support</option>
             </select>
          </div>
          <div class="form-group">

            <label for="BranchSelect1">Branch</label>
            <select class="form-control" name = "BranchSelect1" id="BranchSelect1">
               <?php
                    $host="localhost:3306";
                    $user="natkanok_hr";
                    $pwd="14561";
                    $dbname = "natkanok_lionhearthr";
                    $con = mysqli_connect($host,$user,$pwd,$dbname);
                    if(mysqli_connect_errno())
                    {
                      echo "error";
                      exit();
                    }
                    $result = mysqli_query($con,"SELECT BranchID,State FROM branch");
                    while ($row = mysqli_fetch_array($result))
                    {
                      $id = $row['BranchID'];
                      $name = $row['State'];
                      echo '<option name = "BranchSelect1" id="BranchSelect1" value='.$id.'>'.$name.'</option>';
                    }
              ?>
           <!-- <option name = "BranchSelect1" id="BranchSelect1" value ="BR001">NewYork</option>
            <option name = "BranchSelect1" id="BranchSelect1" value ="BR002">Los Angeles</option>
            <option name = "BranchSelect1" id="BranchSelect1" value ="BR003">Florida</option>
            <option name = "BranchSelect1" id="BranchSelect1" value ="BR004">Hawaii</option>
            <option name = "BranchSelect1" id="BranchSelect1" value ="BR005">Michigan</option> -->
            </select>
    </div>
<!--Education-->
      <h1>
    <img src="assets/img/graduation-cap-solid.svg" width="30" height="30" alt="">
  Education</h1>    
        <div class="form-group">
             <label for="EducationSelect2">Education Level</label>
            <select class="form-control" name = "EducationSelect2" id="EducationSelect2">
              <option name = "EducationSelect2" id="EducationSelect2" value ="Bachelor">Bachelor</option>
              <option name = "EducationSelect2" id="EducationSelect2" value ="Master">Master</option>
              <option name = "EducationSelect2" id="EducationSelect2" value ="Doctoral">Doctoral</option>
            </select>
        </div>

        <div class="form-group">
         <label for="GpaxInput2">GPAX</label>
          <input type="text" class="form-control" name = "GpaxInput2" id="GpaxInput2">
          <label for="DepartmentInput3">Department</label>
          <input type="text" class="form-control" name = "DepartmentInput3" id="DepartmentInput3">
          <label for="FacultyInput4">Faculty</label>
          <input type="text" class="form-control" name = "FacultyInput4" id="FacultyInput4">
          <label for="AcedemyInput5">Academy</label>
          <input type="text" class="form-control" name = "AcedemyInput5" id="AcedemyInput5">
        </div>
<!--Work History-->
      <h1><img src="assets/img/briefcase-solid.svg" width="30" height="30" alt="">
      Work History</h1>
        <div class="form-group">
            <label for="WorkExperiencesInput6">Work Experinces</label>
              <input type="text" class="form-control" name = "WorkExperiencesInput6" id="WorkExperiencesInput6" placeholder="Last duty of your last company.">
              <label for="PositionInputInput7">Position</label>
              <input type="text" class="form-control" name = "PositionInputInput7" id="PositionInputInput7">
              <label for="CompanyNameInput8">Company Name</label>
              <input type="text" class="form-control" name = "CompanyNameInput8" id="CompanyNameInput8">
              <label for="ReasonTextarea1">Reason</label> <br>
              <textarea class="form-control" name = "ReasonTextarea1" id="ReasonTextarea1" rows="3"placeholder="The reason that you resign in your last company."></textarea>
              <label for="DupationInput9">Duration</label>
              <input type="text" class="form-control" name = "DupationInput9" id="DupationInput9">
        </div>
<!--botton-->
  <div class="control-group d-flex justify-content-end">
    <div class="p-2">
        <button type="submit" class="btn btn-navy">Submit</button> <!--link all data to database when push this button-->
    </div>
</div>
</form>  
</div>
   <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $('#datepicker').datepicker();
        $('#datehire').datepicker();
    </script>
</body>
</html>
