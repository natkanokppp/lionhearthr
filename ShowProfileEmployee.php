<?php 
session_start();
ob_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Profile</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="css/hover.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  </head>
  <body class="bg-background">
    <div>
        <?php
        include "navbarEmployee.php";
        ?>
    </div>
    <div class="container p-3">
      <div class="p-5 bg-container my-5 ">
        <h1 class="text-center pb-3">
            Your Profile
        </h1>
        
           
            <?php
                $host="localhost:3306";
                $user="natkanok_hr";
                $pwd="14561";
                $dbname = "natkanok_lionhearthr";
                $con = mysqli_connect($host,$user,$pwd,$dbname);
                if(mysqli_connect_errno())
                {
                  echo "error";
                  exit();
                }
                $user = $_SESSION["Username"];
                
               $result = mysqli_query($con,"SELECT * FROM employee_information WHERE EmployeeID = '$user' ");
               $result2 = mysqli_query($con,"SELECT Country,HouseNo,ZIP FROM employee_address WHERE EmployeeID ='$user'");
  
               $result4 = mysqli_query($con,"SELECT Picture FROM employee_picture WHERE EmployeeID = '$user' 
                AND Picture='1' ");
               ?>
        
            <?php
             /* if ($r4['Picture'] == '1') 
              {
              $abc = "employeePic/".$r4['Picture'].".jpg" ;
              echo "<input type=\"image\" name=\"pic\" id=\"pic\" src=\"$abc\" />";
              echo $abc ;
              }*/

            $path = "employeePic/". $user.".jpg" ;

            while ($row = mysqli_fetch_array($result4))
              {
                 echo "<input type=\"image\" name=\"pic\" id=\"pic\" src=\"$path\" />";
              } 
            while($row = mysqli_fetch_array($result))
              {                  
              
                echo "<h1>" .$row["Name"]  ."<br>" ."</h1>";
                echo "Employee ID: " .$row["EmployeeID"]  ."<br>";
                echo "Position ID: " .$row["PositionID"]  ."<br>";
                echo "Branch ID: " .$row["BranchID"]  ."<br>";
                echo "Department: " .$row["DepartmentName"]  ."<br>";
                echo "Gender: " .$row["Gender"]  ."<br>";
                echo "ID Card: " .$row["IDCard"]  ."<br>";
                echo "Date of Birth: " .$row["DOB"]  ."<br>";
                echo "Phone Number: " .$row["Phone"]  ."<br>";
                echo "E-mail: " .$row["Email"]  ."<br>";
                echo "Nationality: " .$row["Nation"]  ."<br>";
                echo "Religion: " .$row["Religion"]  ."<br>";
                echo "Martital: " .$row["Marital"]  ."<br>";
                echo "Date of hire: " .$row["DateofHire"] ."<br>";
                echo "Bank Account: " .$row["BankAccount"]  ."<br>";
                echo "Password: " .$row["Password"]  ."<br>";
              }
              ?>
              <?php
              while($row = mysqli_fetch_array($result2))
              {
                echo "<h1>" ."Address" ."<br>" ."</h1>";
                echo "Country: " .$row["Country"]  ."<br>";
                echo "House No.: " .$row["HouseNo"]  ."<br>";
                echo "ZIP: " .$row["ZIP"]  ."<br>";
              }
            

                mysqli_close($con);
              ?>
            
            
              <div class="d-flex justify-content-end">
                <a href="ShowWorkHistoryEmployee.php" class="btn btn-navy" role="button">Next</a>
              </div>

        </div>
    </div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>