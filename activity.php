<?php
session_start();
ob_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Activity</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>
    
  </head>
  <body class="bg-background">
    <div>
        <?php
        include "navbar.php";
        ?>
    </div>

      <div class="container p-3">
          <div class="py-5 bg-container my-5">
            <h1 class="text-center pb-3">
                Activity
            </h1>
            <div class="d-flex justify-content-center">
                <form class="form-horizontal" onsubmit="return myFunction_submit()">
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5" for="inputDetail">Detail : </label>
                        <textarea class="form-control col-7" id="inputDetail" name="inputDetail" cols="30" rows="5" placeholder="Activity Detail"></textarea>
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-4" for="datepickerStart">Start Date : </label>
                        <div class="col-8" style="padding-left: 75px;">
                            <input id="datepickerStart" name="datepickerStart" width="276" />
                        </div>
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-4" for="datepickerEnd">End Date : </label>
                        <div class="col-8" style="padding-left: 75px;">
                            <input id="datepickerEnd" name="datepickerEnd" width="276" />
                        </div>
                    </div>
                    <div class="control-group row pt-3 mx-4">
                        <label class="control-label col-5 pb-3" for="chooseStaff">Staff :</label>
                    </div>
                    <table id="example" class="display table table-striped table-bordered mx-2" name="chooseStaff[]" style="width:100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>BranchID</th>
                                    <th>Department</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            include "connect.php";
                            $result_table = mysqli_query($con,"SELECT `EmployeeID`,`Name`,`Phone`,`BranchID`,`DepartmentName` FROM employee_information");
                            while($row = mysqli_fetch_array($result_table))
                            {
                                echo "<tr id=". $row['EmployeeID'] ." onclick='myFunction(this.id)'>";
                                echo "<td>" . $row['EmployeeID'] . "</td>";
                                echo "<td>" . $row['Name'] . "</td>";
                                echo "<td>" . $row['Phone'] . "</td>";
                                echo "<td>" . $row['BranchID'] . "</td>";
                                echo "<td>" . $row['DepartmentName'] . "</td>";
                                echo "</tr>";
                            }
                            mysqli_close($con);
                            ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>BranchID</th>
                                    <th>Department</th>
                                </tr>
                            </tfoot>
                        </table>
                    <div class="control-group row pt-5 d-flex justify-content-center">
                        <button type="submit" class="btn btn-navy">Submit</button>
                    </div>
                </form>
            </div>
          </div>
      </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#datepickerStart').datepicker();
        $('#datepickerEnd').datepicker();
    </script>
    <script>
        var id_array = [];
        $(document).ready(function() {
        $('#example').DataTable( {
            select: {
                style: 'multi'
            }
        } );
    } );

    function myFunction(select_id) {
        var i;
        if(id_array.length != 0)
        {
            var a = id_array.indexOf(select_id);
            if (a >= 0)
            {
                //alert("Some Ja");
                id_array.splice(a, 1);
                //alert(id_array);
                //alert(a);
            }
            else
            {
                id_array.push(select_id);
                //alert(id_array);
                //alert(a);
            }
        }
        else
        {
            id_array.push(select_id);
            //alert(id_array);
        }

    }

    function myFunction_submit(){
        var inputDetail = document.getElementById("inputDetail").value;
        var datepickerStart = document.getElementById("datepickerStart").value;
        var datepickerEnd = document.getElementById("datepickerEnd").value;

        $.ajax({
            type: 'POST',
            url: 'saveactivity.php',
            data: { text1: id_array, inputDetail: inputDetail, datepickerStart: datepickerStart, datepickerEnd: datepickerEnd},
            success: function() {
                alert("Successful!");
                window.location.href = "afterlogin.php";
            }
        });
        return false;
    }
    </script>
  </body>
</html>