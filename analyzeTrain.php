<?php 
session_start();
ob_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Most Attending Course</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/css/gijgo.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.13/combined/js/gijgo.min.js"></script>

   </head>
  <body class="bg-background">
  <div>
      <?php
      include "navbar.php";
      ?>
    </div>

      <div class="container p-3">
          <div class="py-5 bg-container my-5">
            <h1 class="text-center pb-3">
            Most Attending Course Employee
            </h1>
            <div class="d-flex justify-content-center">
                <?php
                    $host="localhost:3306";
                    $user="natkanok_hr";
                    $pwd="14561";
                    $dbname = "natkanok_lionhearthr";
                    $con = mysqli_connect($host,$user,$pwd,$dbname);
                    if(mysqli_connect_errno())
                    {
                      echo "error";
                      exit();
                    }

                    $result = mysqli_query($con,"SELECT e.Name, COUNT(ta.CourseID) AS number
                    FROM employee_information e, training t, training_attendence ta 
                    WHERE e.EmployeeID = ta.EmployeeID AND ta.CourseID = t.CourseID
                    GROUP BY e.EmployeeID
                    ORDER BY number DESC");
                    ?>

                    <table id="example11" class="display table table-striped table-bordered mx-2" style="width:100%">
                    <thead>
                      <tr>
                        <th>Department</th>
                        <th>TotalAttending</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row = mysqli_fetch_array($result))
                      {
                        echo "<tr>";
                        echo "<td>" . $row['Name'] . "</td>";
                        echo "<td>" . $row['number'] . "</td>";
                        echo "</tr>";
                      }
                    echo "</table>";

                    mysqli_close($con);
                  ?>
                  </tbody>
                </table>
            </div>
          </div>
      </div>

    


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/select/1.3.0/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script >
      $(document).ready(function() {
      $('#example11').DataTable(
          {paging: false;}
      );
      } );
    </script>
  </body>
</html>