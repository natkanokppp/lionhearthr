<?
session_start();
unset($_SESSION["Username"]);
unset($_SESSION["wpo"]); 
unset($_SESSION["BranchID"]);  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Log in</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href = "assets/css/bootstrap.min.css">
    <link rel="stylesheet" href = "assets/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
</head>
<body class="bg-background">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="index.html">
        <img src="assets/img/building-solid.svg" width="30" height="30" class="d-inline-block align-top px-1" alt="">
        Lion Heart
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
          </li>
          <li class="nav-item">
            
          </li>
        </ul>
      </div>
  </nav>
<div class="container bg-container">
  <div class="py-5 bg-container my-5">
  <h1 class="text-center pb-3">
		Log in
  </h1>
  <div class="d-flex justify-content-center">
  <form class="form-horizontal" action="login_final.php" method="POST">
    <!-- Username -->
  <div class="control-group row pt-3">
    <label class="control-label col-5" for="inputUsername">Username</label>
      <input class="form-control col-7" type="text" name = "inputUsername" id="inputUsername" placeholder="Enter your username">
  </div>
  <!-- Password -->
  <div class="control-group row pt-3">
    <label class="control-label col-5" for="inputPassword">Password</label>
      <input class = "form-control col-7" type="password" name = "inputPassword" id="inputPassword" placeholder="Enter your password">
  </div>
 <!-- Button -->
  <div class="control-group row pt-5 d-flex justify-content-center">
    <button type="submit" class="btn btn-navy">Sign in</button>
  </div>
  </form>
  </div>
  </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>