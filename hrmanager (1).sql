-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 03, 2019 at 05:23 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrmanager`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `no` int(11) NOT NULL,
  `employeeID` text COLLATE utf8_unicode_ci NOT NULL,
  `firstname` text COLLATE utf8_unicode_ci NOT NULL,
  `lastname` text COLLATE utf8_unicode_ci NOT NULL,
  `gender` text COLLATE utf8_unicode_ci NOT NULL,
  `dateofbirth` date NOT NULL,
  `merital` text COLLATE utf8_unicode_ci NOT NULL,
  `phoneno` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`no`, `employeeID`, `firstname`, `lastname`, `gender`, `dateofbirth`, `merital`, `phoneno`, `email`) VALUES
(1, 'HR0001', 'Phraewadee', 'Chutirat', 'Female', '1974-06-17', 'Single', '0863589643', 'Phraewadee.C@gmail.com'),
(2, 'MN0001', 'Natkanok', 'Poksappaiboon', 'Male', '1963-04-20', 'Divorced', '0874366332', 'Natkanok.P@gmail.com'),
(3, 'ST0001', 'Thidarat', 'Chichana', 'Female', '1978-08-24', 'Married', '092456321', 'Thidarat.Ch@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `leave`
--

CREATE TABLE `leave` (
  `employeeID` text COLLATE utf8_unicode_ci NOT NULL,
  `branchID` text COLLATE utf8_unicode_ci NOT NULL,
  `day` int(11) NOT NULL,
  `startdate` date NOT NULL,
  `stopdate` date NOT NULL,
  `type` text COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leave`
--

INSERT INTO `leave` (`employeeID`, `branchID`, `day`, `startdate`, `stopdate`, `type`, `detail`) VALUES
('HR001', 'BKK01', 2, '2018-09-06', '2018-09-07', 'Sick', 'headache'),
('ST0001', 'BKK01', 3, '2018-02-14', '2018-02-16', 'Business', 'meet somone that important in my life');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employeeID`(32)),
  ADD KEY `no` (`no`);

--
-- Indexes for table `leave`
--
ALTER TABLE `leave`
  ADD PRIMARY KEY (`employeeID`(32));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
