-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 21, 2019 at 06:05 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `natkanok_lionhearthr`
--

-- --------------------------------------------------------

--
-- Table structure for table `bonus_department`
--

CREATE TABLE `bonus_department` (
  `BonusDepID` int(5) NOT NULL,
  `DepartmentName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `BonusRate` int(5) NOT NULL,
  `YearDuration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bonus_position`
--

CREATE TABLE `bonus_position` (
  `BonusPosID` int(5) NOT NULL,
  `PositionID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BonusRate` int(5) NOT NULL,
  `YearDuration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `no` int(5) NOT NULL,
  `BranchID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BranchName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` int(10) NOT NULL,
  `State` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Address` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`no`, `BranchID`, `BranchName`, `Phone`, `State`, `Address`) VALUES
(1, 'BR001', 'LH_NewYork', 26677767, 'NewYork', 'Street 1649 \r\nCity New York\r\nState New York\r\nZip Code 78605'),
(2, 'BR002', 'LH_California', 26696969, 'Los Angeles', 'Street 3177 Brown Bear Drive\r\nCity Los Angeles\r\nState California \r\nZip Code 90017'),
(3, 'BR003', 'LH_Florida', 26886868, 'Florida', 'Street 1514 Robin\r\nCity Auburndale\r\nState Florida\r\nZip Code 33823'),
(4, 'BR004', 'LH_Hawaii', 26656565, 'Hawaii', 'Street 9094 Haunani PL\r\nCity Wailuku\r\nState Hawaii\r\nZip Code 96793'),
(5, 'BR005', 'LH_Michigan', 26636363, 'Michigan', 'Street 3033 Pointe Tremble\r\nCity Algonac\r\nState Michigan\r\nZip Code 48001');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `DepartmentName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `SalaryDepartmentRate` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`DepartmentName`, `SalaryDepartmentRate`) VALUES
('HumanResourceManager', 40000);

-- --------------------------------------------------------

--
-- Table structure for table `education_history`
--

CREATE TABLE `education_history` (
  `EducationNo` int(5) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `EducationLevel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `GPAX` float NOT NULL,
  `Department` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Faculty` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Academy` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_activities`
--

CREATE TABLE `employee_activities` (
  `ActivityID` int(4) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `CourseID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ActivityName` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Result` varchar(1) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_address`
--

CREATE TABLE `employee_address` (
  `AddressID` int(10) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ZIP` int(5) NOT NULL,
  `HouseNo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Country` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_information`
--

CREATE TABLE `employee_information` (
  `no` int(5) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `PositionID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BranchID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `DepartmentName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Gender` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `IDCard` int(15) NOT NULL,
  `DOB` date NOT NULL,
  `Phone` int(10) NOT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Nation` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Religion` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Marital` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `DateofHire` date NOT NULL,
  `BankAccount` int(15) NOT NULL,
  `Password` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_information`
--

INSERT INTO `employee_information` (`no`, `EmployeeID`, `PositionID`, `BranchID`, `DepartmentName`, `Name`, `Gender`, `IDCard`, `DOB`, `Phone`, `Email`, `Nation`, `Religion`, `Marital`, `DateofHire`, `BankAccount`, `Password`) VALUES
(1, 'HR001', '3', 'BR001', 'HumanResourceManager', 'Wang Jackson', 'M', 1122334455, '1994-03-28', 812345678, 'Wang_Jackson@gmail.com', 'USA', 'Buddhism', 'Single', '2019-03-04', 541345268, '123');

-- --------------------------------------------------------

--
-- Table structure for table `employee_picture`
--

CREATE TABLE `employee_picture` (
  `PicID` int(4) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `FileName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `FileSize` int(4) NOT NULL,
  `FileType` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `Picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `user` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `position` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user`, `pwd`, `position`) VALUES
('AD001', '123', 1),
('HR001', '123', 3),
('MN001', '1234', 2),
('ST001', '123', 4);

-- --------------------------------------------------------

--
-- Table structure for table `paid_history`
--

CREATE TABLE `paid_history` (
  `CurrentYear` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `BonusPosID` int(5) NOT NULL,
  `BonusDepID` int(5) NOT NULL,
  `WorkDuration` int(2) NOT NULL,
  `Net` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `PositionID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `PositionName` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `HourBased` int(5) NOT NULL,
  `SalaryRate` int(10) NOT NULL,
  `OTPerHour` int(5) NOT NULL,
  `Tax` int(10) NOT NULL,
  `Insurance` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`PositionID`, `PositionName`, `HourBased`, `SalaryRate`, `OTPerHour`, `Tax`, `Insurance`) VALUES
('3', 'HumanResourceManager', 176, 0, 340, 2000, 1500);

-- --------------------------------------------------------

--
-- Table structure for table `promoted`
--

CREATE TABLE `promoted` (
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `PositionID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Date` date NOT NULL,
  `LastPosition` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `NewPosition` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `take_leave`
--

CREATE TABLE `take_leave` (
  `HolidayID` int(5) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL,
  `Type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Detail` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `CourseID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Detail` text COLLATE utf8_unicode_ci NOT NULL,
  `StartDate` date NOT NULL,
  `EndDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_detail`
--

CREATE TABLE `work_detail` (
  `WDID` int(5) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ArriveTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LeaveTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `work_history`
--

CREATE TABLE `work_history` (
  `WHID` int(5) NOT NULL,
  `EmployeeID` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Company` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Position` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `Reason` text COLLATE utf8_unicode_ci NOT NULL,
  `Duration` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zip_code`
--

CREATE TABLE `zip_code` (
  `ZIP` int(5) NOT NULL,
  `Street` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `City` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bonus_department`
--
ALTER TABLE `bonus_department`
  ADD PRIMARY KEY (`BonusDepID`),
  ADD KEY `DepartmentID` (`DepartmentName`);

--
-- Indexes for table `bonus_position`
--
ALTER TABLE `bonus_position`
  ADD PRIMARY KEY (`BonusPosID`),
  ADD KEY `PositionID` (`PositionID`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`BranchID`),
  ADD UNIQUE KEY `surrogate` (`no`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`DepartmentName`);

--
-- Indexes for table `education_history`
--
ALTER TABLE `education_history`
  ADD PRIMARY KEY (`EducationNo`),
  ADD KEY `EmployeeID` (`EmployeeID`);

--
-- Indexes for table `employee_activities`
--
ALTER TABLE `employee_activities`
  ADD PRIMARY KEY (`ActivityID`),
  ADD KEY `EmployeeID` (`EmployeeID`),
  ADD KEY `CourseID` (`CourseID`);

--
-- Indexes for table `employee_address`
--
ALTER TABLE `employee_address`
  ADD PRIMARY KEY (`AddressID`),
  ADD KEY `EmployeeID` (`EmployeeID`),
  ADD KEY `ZIP` (`ZIP`);

--
-- Indexes for table `employee_information`
--
ALTER TABLE `employee_information`
  ADD PRIMARY KEY (`EmployeeID`),
  ADD UNIQUE KEY `surrogate` (`no`),
  ADD KEY `PositionID` (`PositionID`),
  ADD KEY `BranchID` (`BranchID`),
  ADD KEY `DepartmentID` (`DepartmentName`);

--
-- Indexes for table `employee_picture`
--
ALTER TABLE `employee_picture`
  ADD PRIMARY KEY (`PicID`),
  ADD KEY `EmployeeID` (`EmployeeID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`user`);

--
-- Indexes for table `paid_history`
--
ALTER TABLE `paid_history`
  ADD PRIMARY KEY (`EmployeeID`,`CurrentYear`),
  ADD KEY `BonusPosID` (`BonusPosID`),
  ADD KEY `BonusDepID` (`BonusDepID`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`PositionID`);

--
-- Indexes for table `promoted`
--
ALTER TABLE `promoted`
  ADD PRIMARY KEY (`Date`,`EmployeeID`),
  ADD KEY `EmployeeID` (`EmployeeID`),
  ADD KEY `PositionID` (`PositionID`);

--
-- Indexes for table `take_leave`
--
ALTER TABLE `take_leave`
  ADD PRIMARY KEY (`HolidayID`),
  ADD KEY `EmployeeID` (`EmployeeID`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`CourseID`);

--
-- Indexes for table `work_detail`
--
ALTER TABLE `work_detail`
  ADD PRIMARY KEY (`WDID`),
  ADD KEY `EmployeeID` (`EmployeeID`);

--
-- Indexes for table `work_history`
--
ALTER TABLE `work_history`
  ADD PRIMARY KEY (`WHID`),
  ADD KEY `EmployeeID` (`EmployeeID`);

--
-- Indexes for table `zip_code`
--
ALTER TABLE `zip_code`
  ADD PRIMARY KEY (`ZIP`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `no` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_information`
--
ALTER TABLE `employee_information`
  MODIFY `no` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bonus_department`
--
ALTER TABLE `bonus_department`
  ADD CONSTRAINT `bonus_department_ibfk_1` FOREIGN KEY (`DepartmentName`) REFERENCES `department` (`DepartmentName`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bonus_position`
--
ALTER TABLE `bonus_position`
  ADD CONSTRAINT `bonus_position_ibfk_1` FOREIGN KEY (`PositionID`) REFERENCES `position` (`PositionID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `education_history`
--
ALTER TABLE `education_history`
  ADD CONSTRAINT `education_history_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_activities`
--
ALTER TABLE `employee_activities`
  ADD CONSTRAINT `employee_activities_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_activities_ibfk_2` FOREIGN KEY (`CourseID`) REFERENCES `training` (`CourseID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_address`
--
ALTER TABLE `employee_address`
  ADD CONSTRAINT `employee_address_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_address_ibfk_2` FOREIGN KEY (`ZIP`) REFERENCES `zip_code` (`ZIP`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_information`
--
ALTER TABLE `employee_information`
  ADD CONSTRAINT `employee_information_ibfk_1` FOREIGN KEY (`PositionID`) REFERENCES `position` (`PositionID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_information_ibfk_2` FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_information_ibfk_3` FOREIGN KEY (`DepartmentName`) REFERENCES `department` (`DepartmentName`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_picture`
--
ALTER TABLE `employee_picture`
  ADD CONSTRAINT `employee_picture_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paid_history`
--
ALTER TABLE `paid_history`
  ADD CONSTRAINT `paid_history_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paid_history_ibfk_2` FOREIGN KEY (`BonusPosID`) REFERENCES `bonus_position` (`BonusPosID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paid_history_ibfk_3` FOREIGN KEY (`BonusDepID`) REFERENCES `bonus_department` (`BonusDepID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `promoted`
--
ALTER TABLE `promoted`
  ADD CONSTRAINT `promoted_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `promoted_ibfk_2` FOREIGN KEY (`PositionID`) REFERENCES `position` (`PositionID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `take_leave`
--
ALTER TABLE `take_leave`
  ADD CONSTRAINT `take_leave_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `work_detail`
--
ALTER TABLE `work_detail`
  ADD CONSTRAINT `work_detail_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `work_history`
--
ALTER TABLE `work_history`
  ADD CONSTRAINT `work_history_ibfk_1` FOREIGN KEY (`EmployeeID`) REFERENCES `employee_information` (`EmployeeID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
